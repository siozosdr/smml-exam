from __future__ import division
import math
import numpy as np
import csv
import operator
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer


def Question_1():
    # read data
    train = read_file('./data/redshiftTrain.csv')
    train = np.array(train)
    test = read_file('./data/redshiftTest.csv')
    test = np.array(test)

    selection = train[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
    target_vector = train[:, 10]  # extract target vector
    target_vector = target_vector.astype(np.float)

    test_selection = test[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
    test_target_vector = test[:, [10]]
    test_target_vector = test_target_vector.astype(np.float)
    # perform regression
    w, y = ML_regression(selection, target_vector)

    print('-------Question 1 results:-------')
    # report wrights table
    print('Weights table with bias/offset: ' + str(w))

    # error on test
    xs_test = test_selection.astype(float).tolist()
    ys_test = []
    for xs in xs_test:
        ys_test.append(w[0] + (w[1] * xs[0]) + (w[2]*xs[1]) + (w[3]*xs[2])+
            (w[4]*xs[3]) + (w[5]*xs[4])+(w[6]*xs[5])+(w[7] * xs[6]) + (w[8] * xs[7]) +
            (w[9] + xs[8]) + (w[10] * xs[9]))
    mse_test = MSE(ys_test, test_target_vector)
    print('MSE on test: ' + str(mse_test))

    # error on train
    xs_train = selection.astype(float).tolist()
    ys_train = []
    for xs in xs_train:
        ys_train.append(w[0] + (w[1] * xs[0]) + (w[2]*xs[1]) + (w[3]*xs[2])+
            (w[4]*xs[3]) + (w[5]*xs[4])+(w[6]*xs[5])+(w[7] * xs[6]) + (w[8] * xs[7]) +
            (w[9] + xs[8]) + (w[10] * xs[9]))
    mse_train = MSE(ys_train, target_vector)
    print('MSE on train: ' + str(mse_train))
def Question_2():
    # define number of epochs for neural network training
    no_epochs = 50
    # read data
    train = read_file('./data/redshiftTrain.csv')
    train = np.array(train)
    train = train.astype(np.float)
    test = read_file('./data/redshiftTest.csv')
    test = np.array(test)
    test = test.astype(np.float)
    selection = train[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
    target_vector = train[:, 10]  # extract target vector
    target_vector = target_vector.astype(np.float)
    selection = selection.astype(np.float)
    tstdata = test[:, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]]
    test_target_vector = test[:, 10]
    # create NN and setup
    net = buildNetwork(10, 3, 1)
    dataset = SupervisedDataSet(10, 1)
    for i in range(len(target_vector)):
        dataset.addSample(selection[i],target_vector[i])
    trainer = BackpropTrainer(net, dataset)
    # train NN
    print('-------Question 2 Neural Network results:--------')
    for i in range(1, no_epochs+1):
        trainer.train()
        results = []
        for t in tstdata:
            results.append(net.activate(t))
        list_of_results = []
        for r in results:
            temp = r.tolist()
            list_of_results.append(temp[0])
        # report MSE on test and train sets based on epoch
        mse_test = MSE(list_of_results, test_target_vector)
        print('MSE on test: '+ str(mse_test) + ' with ' + str(i) + ' epochs of training')
        for t in selection:
            results.append(net.activate(t))
        list_of_results = []
        for r in results:
            temp = r.tolist()
            list_of_results.append(temp[0])
        mse_train = MSE(list_of_results, target_vector)
        print('MSE on train: '+ str(mse_train)+ ' with ' + str(i) + ' epochs of training')
    
    # KNN classification
    print('-------Question 2 KNN Results:-------')
    class_index = 10
    for k in range(1, 11, 2):
        # there is no point in returning the accuracy, because it is only used for classification
        _, result = knn(train, test, k, class_index)
        mse_test = MSE(result, test_target_vector)
        print('MSE on test set: ' + str(mse_test) + ' with k: ' + str(k))
        # there is no point in returning the accuracy, because it is only used for classification
        _, result = knn(train, train, k, class_index)
        mse_train = MSE(result, target_vector)
        print('MSE on train set: ' + str(mse_train)+ ' with k: ' + str(k))
def Question_3():
    # read data
    train = read_file2('./data/keystrokesTrainTwoClass.csv')
    test = read_file2('./data/keystrokesTestTwoClass.csv')
    train = np.array(train)
    test = np.array(test)
    train = train.astype(np.float)
    test = test.astype(np.float)
    # calculations required for sensitivity and specificity
    no_positive_classes = 0
    no_negative_classes = 0
    for t in test:
        if t[21] == 1:
            no_positive_classes += 1
        elif t[21] == 0:
            no_negative_classes += 1
    # perform 2-class LDA with train and test sets
    acc, sen, spec = LDATwoClass(train, test)
    print('-------Question 3 LDA Results:-------')
    print('Accuracy on test set: ' + str(acc) + ' with sensitiviy ' + str(sen/no_positive_classes) + ' and specificity ' + str(spec/no_negative_classes))
    no_positive_classes = 0
    no_negative_classes = 0
    for t in train:
        if t[21] == 1:
            no_positive_classes += 1
        elif t[21] == 0:
            no_negative_classes += 1
    # perform 2-class LDA with train and train sets
    acc, sen, spec = LDATwoClass(train, train)
    # report results
    print('Accuracy on train set: ' + str(acc) + ' with sensitiviy ' + str(sen/no_positive_classes) + ' and specificity ' + str(spec/no_negative_classes))
    # SVM part (need libsvm to reproduce the results)
    print('-------Question 3 SVM Results:-------')
    prediction_test = read_file('./data/predictionTwoClass')
    prediction_train = read_file('./data/predictionOnTrainTwoClass')
    prediction_test = np.array(prediction_test)
    prediction_test = prediction_test.flatten()
    prediction_test = [int(x) for x in prediction_test]
    prediction_train = np.array(prediction_train)
    prediction_train = prediction_train.flatten()
    prediction_train = [int(x) for x in prediction_train]
    sensitivity = 0.0
    specificity = 0.0
    for i in range(len(train)):
        if prediction_train[i] == train[i][21]:
            if train[i][21] == 1:
                sensitivity += 1
            elif train[i][21] == 0:
                specificity += 1
    # accuracy is computed by libSVM
    print('Accuracy on train set: 100% with sensitiviy ' + str(sensitivity/no_positive_classes) + ' and specificity ' + str(specificity/no_negative_classes))
    sensitivity = 0.0
    specificity = 0.0
    no_positive_classes = 0.0
    no_negative_classes = 0.0
    for t in test:
        if t[21] == 1:
            no_positive_classes += 1
        elif t[21] == 0:
            no_negative_classes += 1
    for i in range(len(test)):
        if prediction_test[i] == test[i][21]:
            if test[i][21] == 1:
                sensitivity += 1
            elif test[i][21] == 0:
                specificity += 1
    # accuracy is computed by libSVM
    print('Accuracy on test set: 98.75% with sensitiviy ' + str(sensitivity/no_positive_classes) + ' and specificity ' + str(specificity/no_negative_classes))

def Question_6():
    # read data
    train = read_file2('./data/keystrokesTrainMulti.csv')
    test = read_file2('./data/keystrokesTestMulti.csv')
    train = np.array(train)
    test = np.array(test)
    train = train.astype(np.float)
    test = test.astype(np.float)
    # perform multi-class LDA with train and test sets
    acc = LDAMultClass(train, test)
    print('-------Question 6 LDA Results:-------')
    print('Accuracy on test set: ' + str(acc))
    # perform multi-class LDA with train and train sets
    acc = LDAMultClass(train, train)
    print('-------Question 6 LDA Results:-------')
    print('Accuracy on train set: ' + str(acc))
    # KNN part with K=3
    k = 3
    class_index = 21
    acc, _ = knn(train, test, k, class_index)
    print('-------Question 6 KNN Results:-------')
    print('Accuracy on test set: ' + str(acc) + ' with ' + str(k) + ' neighbors.')
    k = 3
    acc, _ = knn(train, train, k, class_index)
    print('-------Question 6 KNN Results:-------')
    print('Accuracy on train set: ' + str(acc) + ' with ' + str(k) + ' neighbors.')

'''
Helpers
'''
# function to calculate MSE
def MSE(y, target):
    mse = 0
    for i in range(0,len(target)):
         mse += (target[i] - y[i])**2
    mse /= len(target)
    return mse

# Maximum Likelihood regression implementation
def ML_regression(selection, target):
    (r, c) = selection.shape
    ones_vector = [1] * r
    ones_vector = np.reshape(ones_vector, (r, 1))
    phi = np.column_stack((ones_vector, selection)) # create phi_table based on the selection table
    phi = phi.astype(np.float)
    phi_t = phi.T
    phi_t_dot_phi = np.dot(phi_t, phi)
    phi_t_dot_phi_inv = np.linalg.inv(phi_t_dot_phi)
    weights_table = np.dot(phi_t_dot_phi_inv, phi_t)
    weights_table = np.dot(weights_table, target)
    weights_table = weights_table.T
    y_table = []
    for i in range(0, len(target)):
        y_table.append(np.dot(weights_table, phi[i]))
    return weights_table, y_table

# returns a mean vector
def mean(ds):
    ds = np.array(ds)
    result = ds.mean(axis=0)
    result = np.reshape(result, (1, 22))
    return result

# returns covariance
def covariance(ds, mu):
    r = 0
    mu = np.reshape(mu, (22, 1))
    mu = mu[0:21]
    mu = np.reshape(mu, (1, 21))
    for x in ds:
        v = x[0:21]
        v = np.reshape(v, (1, 21))
        a = np.subtract(v, mu)
        b = a[None].T
        c = np.multiply(a, b)
        # finally, sum it up
        r += c
    return r

# Multiple-class LDA implementation
def LDAMultClass(train, test):
    cs = {} # classification groups

    # split the training data into classification groups

    for t in train:
        c = int(t[21])
        try:
            cs[c].append(t)
        except KeyError:
            cs[c] = []

    '''
    '''
    # means and covariances
    means = []
    means.append(np.array(mean(cs[1])))
    means.append(np.array(mean(cs[2])))
    means.append(np.array(mean(cs[3])))
    means.append(np.array(mean(cs[4])))
    means.append(np.array(mean(cs[5])))

    c_1 = covariance(cs[1], means[0])
    c_2 = covariance(cs[2], means[1])
    c_3 = covariance(cs[3], means[2])
    c_4 = covariance(cs[4], means[3])
    c_5 = covariance(cs[5], means[4])
    c_1 = np.reshape(c_1, (21, 21))
    c_2 = np.reshape(c_2, (21, 21))
    c_3 = np.reshape(c_3, (21, 21))
    c_4 = np.reshape(c_4, (21, 21))
    c_5 = np.reshape(c_5, (21, 21))
    # sum covs up and divide
    c = (1 / (len(train) - 5)) * (c_1 + c_2 + c_3 + c_4 + c_5)

    total_acc = 0.0

    for t in test:

        actual_class = int(t[21])
        mxs = []
        t = t[0:21]
        t = np.array(t)
        x = t.astype(np.float)
        c_inv = np.linalg.inv(c)

        for i in range(0, 5):
            # calculate delta for k=i
            inner_dot = np.dot(x, c_inv)
            temp = means[i][None].T
            # remove class label from array
            temp = np.reshape(temp, (22, 1))
            temp = temp[0:21]

            delta = np.dot(inner_dot, temp)
            delta = np.reshape(delta, (1, 1))
            temp_means = means[i]
            # remove class label from array
            temp_means = np.reshape(temp_means, (22, 1))
            temp_means = temp_means[0: 21]
            temp_means = np.reshape(temp_means, (1, 21))

            inner_dot = np.dot(temp_means, c_inv)
            to_be_added = -0.5 * np.dot(inner_dot, temp)
            delta += to_be_added
            delta += math.log(len(cs[i+1]) / len(train))

            mxs.append(delta)

        if np.argmax(mxs) == actual_class:
            total_acc += 1

    return total_acc/len(test)

# Two-class LDA implementation
def LDATwoClass(train, test):
    sensitivity = 0.0
    specificity = 0.0
    cs = {} # classification groups

    # split the training data into classification groups

    for t in train:
        c = int(t[21])
        try:
            cs[c].append(t)
        except KeyError:
            cs[c] = []

    # means and covariances
    means = []
    means.append(np.array(mean(cs[0])))
    means.append(np.array(mean(cs[1])))

    c_1 = covariance(cs[0], means[0])
    c_2 = covariance(cs[1], means[1])
    c_1 = np.reshape(c_1, (21, 21))
    c_2 = np.reshape(c_2, (21, 21))
    # sum covs up and divide
    c = (1 / (len(train) - 2)) * (c_1 + c_2)

    total_acc = 0.0

    for t in test:

        actual_class = int(t[21])
        mxs = []
        t = t[0:21]
        t = np.array(t)
        x = t.astype(np.float)
        c_inv = np.linalg.inv(c)

        for i in range(0, 2):
            # calculate delta for k=i
            inner_dot = np.dot(x, c_inv)
            temp = means[i][None].T
            temp = np.reshape(temp, (22, 1))
            temp = temp[0:21]
            delta = np.dot(inner_dot, temp)
            delta = np.reshape(delta, (1, 1))
            temp_means = means[i]
            temp_means = np.reshape(temp_means, (22, 1))
            temp_means = temp_means[0: 21]
            temp_means = np.reshape(temp_means, (1, 21))
            inner_dot = np.dot(temp_means, c_inv)
            to_be_added = -0.5 * np.dot(inner_dot, temp)
            delta += to_be_added
            delta += math.log(len(cs[i]) / len(train))

            mxs.append(delta)

        if np.argmax(mxs) == actual_class:
            total_acc += 1
            if actual_class == 1:
                sensitivity += 1
            elif actual_class == 0:
                specificity += 1

    return total_acc/len(test), sensitivity, specificity

# KNN implementation for both classification and regression
def knn(train, test, k, class_index):
    # total_acc is meaningful when we use knn for classification
    total_acc = 0
    # result is meaningful when we use knn for regression
    result = []
    
    for t in test:
        neighbor_classes = []
        nns = getNeighbors(train, t, k)
        acc = 0
        c_t = t[class_index]

        # calculate the accuracy
        for nn in nns:
            neighbor_classes.append(nn[class_index])
            if nn[class_index] == c_t:
                acc += 1

        if acc / k > 0.5:
            total_acc += 1

        # keep as result the average of the ground truths of the neighbors (meaningful in regression only)
        result.append(sum(neighbor_classes)/len(neighbor_classes))

    return total_acc / len(test), result

# helper function for KNN to get top K neighbors
def getNeighbors(trainingSetVectors, testInstance, k):
    distances = []
    for x in range(len(trainingSetVectors)):
        # compute distances for each neighbor
        dist = distance(testInstance, trainingSetVectors[x])
        distances.append((trainingSetVectors[x], dist))
    # sort based on distance
    distances.sort(key=operator.itemgetter(1))

    neighbors = []
    # get top k
    for x in range(k):
        neighbors.append(distances[x][0])
    return neighbors

# Euclidean distance for knn
def distance(v1, v2):
    result = 0
    for i in range(len(v1)):
        x = v1[i]
        y = v2[i]
        result += (x-y)**2
    return math.sqrt(result)

# helper function to read the redshift files
def read_file(filename):
    data = []
    with open(filename) as rows:
        for row in csv.reader(rows, delimiter=' '):
            data.append(row)
    return data

# helper function to read the keystrokes files
def read_file2(filename):
    data = []
    with open(filename) as rows:
        for row in csv.reader(rows, delimiter=','):
            data.append(row)
    return data

if __name__ == '__main__':
    Question_1()
    Question_2()
    Question_3()
    Question_6()