# README

This repo contains the implementation of the course Statistical Methods for Machine Learning from the MSc in Computer Science program of DIKU.

## General
The repo contains implementaion of different machine learning methods learned throughout the course including Maximum Likelihood Linear Regression, non-linear regression,
a neural network (using the library pybrain) and the KNN algorithm. In addition, it contains the implementation of binary classification using Linear Discriminant Analysis (LDA),
and an SVM for a non-linear one. Finally, Pricipal Component Analysis (PCA) and kmeans was implemented using MATLAB.

## Prerequisites:##
- Python version:  > 2.7.5
- Python libraries: pybrain, operator, csv, math, numpy
- Programs: libSVM-3.20 (SVM implementation), matlab (pca implementation)