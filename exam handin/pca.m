function [train, test, index] = pca(train,test)
%[train, test] = pca(train,test);
%% PCA
 mean_tr_feats=mean(train);
 normalized_tr_feats=train-repmat(mean_tr_feats,length(train),1);
 cov=normalized_tr_feats'*normalized_tr_feats;
 [V,D]=eig(cov);
 [value,index]=sort(diag(D),'descend');
 V_proj=V(:,index(1:2));       % only retain the top 2 dimensions
 train=normalized_tr_feats*V_proj;
 test=(test-repmat(mean_tr_feats,length(test),1))*V_proj;
end