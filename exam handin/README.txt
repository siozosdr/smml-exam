Prerequisites:
- Python version:  > 2.7.5
- Python libraries: pybrain, operator, csv, math, numpy
- Programs: libSVM-3.20 (SVM implementation), matlab (pca implementation)